/*import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});*/

/*import MeteorOffline from '@ajaybhatia/react-native-meteor-redux';
const GroundedMeteor = new MeteorOffline({debounce: 1000});
export {GroundedMeteor};*/



import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, Text, Linking, SafeAreaView } from 'react-native';
import Meteor, { withTracker, useTracker  } from '@ajaybhatia/react-native-meteor';
import { List, ListItem, Icon } from 'react-native-elements'

import { initializeMeteorOffline } from './react-native-meteor-offline';

Meteor.connect('ws://192.168.1.112:3000/websocket'); 
//initializeMeteorOffline({ log: true });

const data = [
  {
    title: 'Meteor',
    url: 'https://www.meteor.com',
  },
  {
    title: 'Learn React Native + Meteor',
    url: 'http://learn.handlebarlabs.com/p/react-native-meteor',
  },
  {
    title: 'React Native',
    url: 'http://facebook.github.io/react-native/',
  }
];

class RNDemo extends Component {

//const RNDemo = ({status, links, linksExists}) => {

  constructor(props) {
    super(props);
    this.listRef = React.
    createRef();
    this.state = {};
  }

  static getDerivedStateFromProps(nextProps, prevState) {
   // console.log(nextProps, prevState)
    return {}
  }
   
  getAllItems = (status, links) => {
    
    return ( 
      <View style={{ backgroundColor: '#f8f8f8' }}>
          
            
              <ListItem
                title="Connection Status"
                rightTitle={this.props.status}
                hideChevron
              />
              {console.log('CONEXAO: ' + status)}
           
          
              {this.props.links.map((link) => {
                console.log('link: ' + link.title)
                return( 
                  <ListItem
                    key={link._id}
                    title={link.title}
                    subtitle={link.url}
                    onPress={() => this.pressItem(link.url)}
                  />
                );
              })}
           
          
          <Icon 
            raised
            name='plus'
            type='font-awesome'
            color='#00aced'
            onPress={this.addItem}
            containerStyle={{ position: 'absolute', bottom: 30, right: 20 }}
            disabled
          />
          <Text>Open up App.js to start working on your app!</Text>
        </View>
    )
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
      <ScrollView>
        {this.props.links?this.getAllItems(this.props.status, this.props.links):<Text>NOT READY</Text>}
      </ScrollView>
      </SafeAreaView>
    );
  //}
} 
}


addItem = () => {
  const item = data[Math.floor(Math.random() * data.length)];
  Meteor.call('links.insert', item.title, item.url, (error) => {
    if (error) {
      console.log('Insert error', error.error);
    }
  });
};

pressItem = (url) => {
  Linking.canOpenURL(url)
    .then((supported) => {
      if (supported) {
        Linking.openURL(url);
      }
    })
    .catch((err) => console.log('Linking error: ', err));
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
}); 

export default withTracker(params => {

  const linksHandle = Meteor.subscribe('links');
  const loading = !linksHandle.ready();
  const linksExists = !loading; 

  /*const sub = GroundedMeteor.subscribe('getUsersById', 'links', {}, () => {
    console.log('callback');
  });*/
 
  /*const handle = Meteor.subscribe('links');
  Meteor.subscribe('links.all');*/
  
 // Meteor.subscribe('links');

  return {
    linksExists,
    links: Meteor.collection('links').find({}, { sort: { createdAt: -1 } }),
    status: Meteor.status(),
    //docs: GroundedMeteor.collection('links', 'getUsersById').find({}),
  };
})(RNDemo);



/*
import React from 'react';
import { View, Text, FlatList } from 'react-native';
import Meteor, { withTracker } from '@ajaybhatia/react-native-meteor';
 
Meteor.connect('ws://192.168.1.112:3000/websocket'); //do this only once
 
const App = ({ settings, todos, todosReady }) => {
  return (
    <View>
      <Text>{'teste'}</Text>
      {!todosReady && <Text>Not ready</Text>}
 
      <FlatList
        data={todos}
        keyExtractor={item => item._id}
        renderItem={({ item }) => <Text>{item.title}</Text>}
      />
    </View>
  );
};
 
export default withTracker(params => {
  const handle = Meteor.subscribe('links');
  Meteor.subscribe('links.all');
 
  return {
    todosReady: handle.ready(),
    todos: Meteor.collection('links').find({}, { sort: { createdAt: -1 } }),
    settings: Meteor.collection('links.all').findOne(),
  };
})(App);*/